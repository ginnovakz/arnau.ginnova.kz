$(document).ready(function () {
    $(document).on('click','.js-burger-link', function () {
        event.preventDefault();
        $('body').addClass('menu-opened');
        $(document).on('click','.js-burger-close', function () {
            event.preventDefault();
            $('body').removeClass('menu-opened');
        });
    });
    
    $(document).on('click', '.js-overlay-link', function (e) {
        event.preventDefault();
        var overlay = $('.js-overlay-b');
        overlay.fadeIn(600);
        $('body').addClass('no-scroll');

        overlay.click(function(e) {
            if($(e.target).closest('.overlay').length==0)
            {
                overlay.fadeOut(600);
                $('body').removeClass('no-scroll');
            }
        });

        $(document).on('click','.js-overlay-close', function () {
            event.preventDefault();
            overlay.fadeOut(600);
            $('body').removeClass('no-scroll');
        });
        e.stopPropagation();
    });

    $('.input--phone').mask('+7 999 9999999');

    if ($(window).width() > 860) {
        var s =skrollr.init();
    }

    // disable skrollr if the window is resized below 768px wide
    $(window).on('resize', function () {
        if ($(window).width() <= 860) {
            skrollr.init().destroy(); // skrollr.init() returns the singleton created above
        }
    });

    $('.form-submit').submit( function() {
        var text = $(this).find('input[type=text]');
        var tel = $(this).find('input[type=tel]');
        var mail = $(this).find('input[type=email]');

        if ( (text.val() == '') || (tel.val() == '') || (tel.val().match('_')) || (mail.val() == '') ) {
            if ( text.val() == '' ) { text.removeClass('error-phone').addClass('error-phone') }
            else { text.removeClass('error-phone') }

            if ( (tel.val() == '') || (tel.val().match('_')) ) { tel.removeClass('error-phone').addClass('error-phone') }
            else { tel.removeClass('error-phone') }

            if ( (mail.val() == '') ) { mail.removeClass('error-phone').addClass('error-phone') }
            else { tel.removeClass('error-phone') }

            return false;
        }
        else { text.removeClass('error-phone'); tel.removeClass('error-phone '); mail.removeClass('error-phone '); }
    });
    
});