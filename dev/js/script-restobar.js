$(document).ready(function () {

    $(document).on('click','.js-burger-link', function () {
        event.preventDefault();
        $('body').addClass('menu-opened');
        $(document).on('click','.js-burger-close', function () {
            event.preventDefault();
            $('body').removeClass('menu-opened');
        });
    });
    
    $(document).on('click', '.js-overlay-link', function (e) {
        event.preventDefault();
        var overlay = $('.js-overlay-b');
        overlay.fadeIn(600);
        $('body').addClass('no-scroll');

        overlay.click(function(e) {
            if($(e.target).closest('.overlay').length==0)
            {
                overlay.fadeOut(600);
                $('body').removeClass('no-scroll');
            }
        });

        $(document).on('click','.js-overlay-close', function () {
            event.preventDefault();
            overlay.fadeOut(600);
            $('body').removeClass('no-scroll');
        });
        e.stopPropagation();
    });

    $('.input--phone').mask('+7 999 9999999');

    $('.about-slider')
        .on('init', function(event, slick){

        })
        .on('afterChange', function(event, slick, currentSlide){
            var prev_title_text = $(this).find('.slick-slide:eq(' + ( currentSlide - 1 ) + ')').find('.paintball-service__title').text();

            var length = slick.slideCount;
            if (currentSlide == (length - 1) ) {
                var next_title_text = $(this).find('.slick-slide:eq(' + ( currentSlide - ( length - 1) ) + ')').find('.paintball-service__title').text();
            }
            else {
                var next_title_text = $(this).find('.slick-slide:eq(' + ( currentSlide + 1 ) + ')').find('.paintball-service__title').text();
            }
            $('.about-slider__arrow-text--next').html(next_title_text);
            $('.about-slider__arrow-text--prev').html(prev_title_text);
        }).
        slick({
        dots: true,
        infinite: true,
        speed: 500,
        fade: true,
        cssEase: 'linear',
        nextArrow: '<span class="about-slider__arrow about-slider__arrow--next float"><span class="about-slider__arrow-text about-slider__arrow-text--next">Вип-комнаты</span><svg class="about-slider__arrow-icon" version="1.1" x="0px" y="0px" viewBox="0 0 15 10" width="15px" height="10px"><line fill="none" stroke="#D39C4A" stroke-width="2" stroke-miterlimit="10" x1="13.5" y1="4.9" x2="0" y2="4.9"/><polyline fill="none" stroke="#D39C4A" stroke-width="1.5" stroke-miterlimit="10" points="9.8,0.6 13.8,4.9 9.8,9.4 "/></svg></span>',
        prevArrow: '<span class="about-slider__arrow about-slider__arrow--prev float"><span class="about-slider__arrow-text about-slider__arrow-text--prev">Тематический дизайн</span><svg class="about-slider__arrow-icon" version="1.1" x="0px" y="0px" viewBox="0 0 15 10" width="15px" height="10px"><line fill="none" stroke="#D39C4A" stroke-width="2" stroke-miterlimit="10" x1="13.5" y1="4.9" x2="0" y2="4.9"/><polyline fill="none" stroke="#D39C4A" stroke-width="1.5" stroke-miterlimit="10" points="9.8,0.6 13.8,4.9 9.8,9.4 "/></svg></span>'
    });

    $('.interior__slider')
        .on('init reInit afterChange', function (event, slick, currentSlide, nextSlid){
            var i = (currentSlide ? currentSlide : 0) + 1;
            $('.interior__slider-current').html(i);
            $('.interior__slider-count').html('/'+slick.slideCount);
        })
        .slick({
            dots: true,
            infinite: true,
            speed: 500,
            fade: true,
            cssEase: 'linear',
            nextArrow: '<span class="interior__slider-arrow-b interior__slider-arrow--next"><span class="interior__slider-count"></span><svg class="interior__slider-arrow" version="1.1" x="0px" y="0px" viewBox="0 0 15 10" width="15px" height="10px"><line fill="none" stroke="#ffffff" stroke-width="2" stroke-miterlimit="10" x1="13.5" y1="4.9" x2="0" y2="4.9"/><polyline fill="none" stroke="#ffffff" stroke-width="1.5" stroke-miterlimit="10" points="9.8,0.6 13.8,4.9 9.8,9.4 "/></svg></span>',
            prevArrow: '<span class="interior__slider-arrow-b interior__slider-arrow--prev"><svg class="interior__slider-arrow interior__slider-arrow-transform" version="1.1" x="0px" y="0px" viewBox="0 0 15 10" width="15px" height="10px"><line fill="none" stroke="#ffffff" stroke-width="2" stroke-miterlimit="10" x1="13.5" y1="4.9" x2="0" y2="4.9"/><polyline fill="none" stroke="#ffffff" stroke-width="1.5" stroke-miterlimit="10" points="9.8,0.6 13.8,4.9 9.8,9.4 "/></svg><span class="interior__slider-current"></span></span>'
        });

    $('.discount__slider').slick({
        dots: true,
        infinite: true,
        speed: 500,
        fade: true,
        customPaging : function(slider, i) {
            var title = $(slider.$slides[i]).data('title');
            return title;
        },
        cssEase: 'linear',
        arrows: false
    });

    lightbox.option({
        albumLabel: 'Изображение %1 из %2'
    });

    $('.form-submit').submit( function() {
        var text = $(this).find('input[type=text]');
        var tel = $(this).find('input[type=tel]');
        var mail = $(this).find('input[type=email]');

        if ( (text.val() == '') || (tel.val() == '') || (tel.val().match('_')) || (mail.val() == '') ) {
            if ( text.val() == '' ) { text.removeClass('error-phone').addClass('error-phone') }
            else { text.removeClass('error-phone') }

            if ( (tel.val() == '') || (tel.val().match('_')) ) { tel.removeClass('error-phone').addClass('error-phone') }
            else { tel.removeClass('error-phone') }

            if ( (mail.val() == '') ) { mail.removeClass('error-phone').addClass('error-phone') }
            else { tel.removeClass('error-phone') }

            return false;
        }
        else { text.removeClass('error-phone'); tel.removeClass('error-phone '); mail.removeClass('error-phone '); }
    });

    $(document).on('click','.js-menu-link',function (e) {
        event.preventDefault();
        var overlay = $('.js-menu-b');
        overlay.fadeIn(600);
        $('body').addClass('no-scroll');

        overlay.click(function(e) {
            if($(e.target).closest('.overlay').length==0)
            {
                overlay.fadeOut(600);
                $('body').removeClass('no-scroll');
            }
        });

        $(document).on('click','.js-menu-close', function () {
            event.preventDefault();
            overlay.fadeOut(600);
            $('body').removeClass('no-scroll');
        });
        e.stopPropagation();
    });
});