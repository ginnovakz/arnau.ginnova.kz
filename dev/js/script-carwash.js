$(document).ready(function () {
    $(document).on('click','.js-burger-link', function () {
        event.preventDefault();
        $('body').addClass('menu-opened');
        $(document).on('click','.js-burger-close', function () {
            event.preventDefault();
            $('body').removeClass('menu-opened');
        });
    });
    
    $(document).on('click', '.js-overlay-link', function (e) {
        event.preventDefault();
        var overlay = $('.js-overlay-b');
        overlay.fadeIn(600);
        $('body').addClass('no-scroll');

        overlay.click(function(e) {
            if($(e.target).closest('.overlay').length==0)
            {
                overlay.fadeOut(600);
                $('body').removeClass('no-scroll');
            }
        });

        $(document).on('click','.js-overlay-close', function () {
            event.preventDefault();
            overlay.fadeOut(600);
            $('body').removeClass('no-scroll');
        });
        e.stopPropagation();
    });

    $('.input--phone').mask('+7 999 9999999');
    
    new WOW({mobile: 'false'}).init();

    $('.services-type__slider')
        .on('init', function(event, slick){

        })
        .on('afterChange', function(event, slick, currentSlide){
            var length = slick.slideCount;
            var next_slide_index = currentSlide + 1; console.log(next_slide_index);

            if (next_slide_index == length ) {
                var next_title_text = $(this).find('.slick-slide:eq(0)').data('title');
            }
            else {
                var next_title_text = $(this).find('.slick-slide:eq('+ next_slide_index +')').data('title');
            }
            $('.js-service-type-arrow-text').html(next_title_text);
        }).
        slick({
        dots: true,
        infinite: true,
        speed: 500,
        fade: true,
        cssEase: 'linear',
        customPaging : function(slider, i) {
            var title = $(slider.$slides[i]).data('title');
            return title;
        },
        prevArrow: '',
        nextArrow: '<span class="services-type__slider-arrow float"><span class="services-type__slider-arrow-text js-service-type-arrow-text">Мойка двигателя</span><svg class="services-type__slider-arrow-icon" version="1.1" x="0px" y="0px" viewBox="0 0 15 10" width="15px" height="10px"><line fill="none" stroke="#D39C4A" stroke-width="2" stroke-miterlimit="10" x1="13.5" y1="4.9" x2="0" y2="4.9"/><polyline fill="none" stroke="#D39C4A" stroke-width="1.5" stroke-miterlimit="10" points="9.8,0.6 13.8,4.9 9.8,9.4 "/></svg></span>'
    });

    $(document).on('click','.js-calculator-link', function () {
        event.preventDefault();
        switch_calc();
    });

    function switch_calc() {
        var car_type = $('#car-type-select').val();
        var service_type = $('#car-service-select').val();
        var result = $('.js-calculator-result');
        switch(car_type) {
            case 'car-passengers':
                switch(service_type) {
                    case 'cleaning':
                        result.html('15 000');
                        break
                    case 'engine-washing':
                        result.html('1 500');
                        break
                    case 'car-wash':
                        result.html('1 900');
                        break
                    case 'salon-polishing':
                        result.html('400');
                        break
                    case 'waxing':
                        result.html('500');
                        break
                }
                break
            case 'car-business':
                switch(service_type) {
                    case 'cleaning':
                        result.html('25 000');
                        break
                    case 'engine-washing':
                        result.html('2 200');
                        break
                    case 'car-wash':
                        result.html('2 200');
                        break
                    case 'salon-polishing':
                        result.html('500');
                        break
                    case 'waxing':
                        result.html('500');
                        break
                }
                break
            case 'car-suv':
                switch(service_type) {
                    case 'cleaning':
                        result.html('40 000');
                        break
                    case 'engine-washing':
                        result.html('3 000');
                        break
                    case 'car-wash':
                        result.html('2 500');
                        break
                    case 'salon-polishing':
                        result.html('800');
                        break
                    case 'waxing':
                        result.html('500');
                        break
                }
                break
        }
        $('.js-calculator-b').fadeIn(400);
    }

    $('.form-submit').submit( function() {
        var text = $(this).find('input[type=text]');
        var tel = $(this).find('input[type=tel]');
        var mail = $(this).find('input[type=email]');

        if ( (text.val() == '') || (tel.val() == '') || (tel.val().match('_')) || (mail.val() == '') ) {
            if ( text.val() == '' ) { text.removeClass('error-phone').addClass('error-phone') }
            else { text.removeClass('error-phone') }

            if ( (tel.val() == '') || (tel.val().match('_')) ) { tel.removeClass('error-phone').addClass('error-phone') }
            else { tel.removeClass('error-phone') }

            if ( (mail.val() == '') ) { mail.removeClass('error-phone').addClass('error-phone') }
            else { tel.removeClass('error-phone') }

            return false;
        }
        else { text.removeClass('error-phone'); tel.removeClass('error-phone '); mail.removeClass('error-phone '); }
    });
});