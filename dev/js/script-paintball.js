$(document).ready(function () {
    $(document).on('click','.js-burger-link', function () {
        event.preventDefault();
        $('body').addClass('menu-opened');
        $(document).on('click','.js-burger-close', function () {
            event.preventDefault();
            $('body').removeClass('menu-opened');
        });
    });
    
    $(document).on('click', '.js-overlay-link', function (e) {
        event.preventDefault();
        var overlay = $('.js-overlay-b');
        overlay.fadeIn(600);
        $('body').addClass('no-scroll');

        overlay.click(function(e) {
            if($(e.target).closest('.overlay').length==0)
            {
                overlay.fadeOut(600);
                $('body').removeClass('no-scroll');
            }
        });

        $(document).on('click','.js-overlay-close', function () {
            event.preventDefault();
            overlay.fadeOut(600);
            $('body').removeClass('no-scroll');
        });
        e.stopPropagation();
    });

    $('.input--phone').mask('+7 999 9999999');

    $('.paintball-service__bg').viewportChecker({
        offset: 150,
        callbackFunction: function (elem) {
            var elem_data = elem.attr('data-view'); console.log(elem_data);
            elem.find('.paintball-service__bg-img').attr('src','/img/paintball-gif-'+ elem_data+'.gif?'+ Math.random()+'');
        }
    });

    $('.paintball-top-in').viewportChecker({
        offset: 0,
        callbackFunction: function (elem) {
            elem.find('.paintball-top__img').attr('src','/img/paintball-gif.gif?'+ Math.random() +'');
        }
    });

    $('.interior__slider')
        .on('init reInit afterChange', function (event, slick, currentSlide, nextSlid){
            var i = (currentSlide ? currentSlide : 0) + 1;
            $('.interior__slider-current').html(i);
            $('.interior__slider-count').html('/'+slick.slideCount);
        })
        .slick({
            dots: true,
            infinite: true,
            speed: 500,
            fade: true,
            cssEase: 'linear',
            nextArrow: '<span class="interior__slider-arrow-b interior__slider-arrow--next"><span class="interior__slider-count"></span><svg class="interior__slider-arrow" version="1.1" x="0px" y="0px" viewBox="0 0 15 10" width="15px" height="10px"><line fill="none" stroke="#ffffff" stroke-width="2" stroke-miterlimit="10" x1="13.5" y1="4.9" x2="0" y2="4.9"/><polyline fill="none" stroke="#ffffff" stroke-width="1.5" stroke-miterlimit="10" points="9.8,0.6 13.8,4.9 9.8,9.4 "/></svg></span>',
            prevArrow: '<span class="interior__slider-arrow-b interior__slider-arrow--prev"><svg class="interior__slider-arrow interior__slider-arrow-transform" version="1.1" x="0px" y="0px" viewBox="0 0 15 10" width="15px" height="10px"><line fill="none" stroke="#ffffff" stroke-width="2" stroke-miterlimit="10" x1="13.5" y1="4.9" x2="0" y2="4.9"/><polyline fill="none" stroke="#ffffff" stroke-width="1.5" stroke-miterlimit="10" points="9.8,0.6 13.8,4.9 9.8,9.4 "/></svg><span class="interior__slider-current"></span></span>'
        });

    lightbox.option({
        albumLabel: 'Изображение %1 из %2'
    });

    $('.form-submit').submit( function() {
        var text = $(this).find('input[type=text]');
        var tel = $(this).find('input[type=tel]');
        var mail = $(this).find('input[type=email]');

        if ( (text.val() == '') || (tel.val() == '') || (tel.val().match('_')) || (mail.val() == '') ) {
            if ( text.val() == '' ) { text.removeClass('error-phone').addClass('error-phone') }
            else { text.removeClass('error-phone') }

            if ( (tel.val() == '') || (tel.val().match('_')) ) { tel.removeClass('error-phone').addClass('error-phone') }
            else { tel.removeClass('error-phone') }

            if ( (mail.val() == '') ) { mail.removeClass('error-phone').addClass('error-phone') }
            else { tel.removeClass('error-phone') }

            return false;
        }
        else { text.removeClass('error-phone'); tel.removeClass('error-phone '); mail.removeClass('error-phone '); }
    });
});