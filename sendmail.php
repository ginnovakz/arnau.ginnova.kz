<?php
require 'vendor/autoload.php';

$mail = new PHPMailer;

//$mail->SMTPDebug = 3;                               // Enable verbose debug output

$mail->isSMTP();                                      // Set mailer to use SMTP
$mail->Host = 'smtp.yandex.ru';  // Specify main and backup SMTP servers
$mail->SMTPAuth = true;                               // Enable SMTP authentication
$mail->Username = 'test@ginnova.kz';

$mail->SMTPOptions = ['ssl'=> [
    'verify_peer' => false,
    'verify_peer_name' => false,
    'allow_self_signed' => true
]];

$mail->Password = 'testtest';                           // SMTP password
$mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
$mail->Port = 465;
$mail->CharSet = 'UTF-8';


$mail->setFrom('test@ginnova.kz', 'Arnau Mailer');
$mail->addAddress('assetknock@gmail.com', 'Arnau Mail');     // Add a recipient
$mail->addAddress('info@ginnova.kz', 'Arnau Mail');     // Add a recipient


//$mail->addAddress('info@ginnova.kz', 'Arnau Mail');     // Add a recipient


$mail->isHTML(true);


if (isset($_POST["text"])){
    $html = '<br><br>
<table cellpadding="5">
<tr>
    <td><b>Имя</b></td>
    <td>'.$_POST["name"].'</td>
</tr>
<tr>
    <td><b>Телефон</b></td>
    <td>'.$_POST["phone"].'</td>
</tr>
<tr>
    <td><b>Email</b></td>
    <td>'.$_POST["email"].'</td>
</tr>
<tr>
    <td><b>Сообщение</b></td>
    <td>'.$_POST["text"].'</td>
</tr>
</table>';
}
else {
    $html = '<br><br>
<table cellpadding="5">
<tr>
    <td><b>Имя</b></td>
    <td>'.$_POST["name"].'</td>
</tr>
<tr>
    <td><b>Телефон</b></td>
    <td>'.$_POST["phone"].'</td>
</tr>
<tr>
    <td><b>Email</b></td>
    <td>'.$_POST["email"].'</td>
</tr>
</table>';
}



$mail->Subject = 'Заявка с сайта Arnau';
$mail->Body    = 'У вас новая заявка с сайта Arnau'.$html;
$mail->AltBody = 'Заявка сгенерирована автоматически';

if(!$mail->send()) {
    echo 'Message could not be sent.';
    echo 'Mailer Error: ' . $mail->ErrorInfo;
} else {
    echo '<div style="font-size: 20px; text-align: center; margin: 20px 0; color: #5c90bc; font-weight: 700;font-family: sans-serif;">Ваша заявка отправлена</div>';
    header('Refresh:3; ' . $_SERVER['HTTP_REFERER']);
}