'use strict';

module.exports = function(grunt) {

    var globalConfig = {
        styles : 'app/css',
        scripts : 'app/js'
    };

    grunt.initConfig({
        globalConfig : globalConfig,
        pkg : grunt.file.readJSON('package.json'),
        concat: {
            dist_mainpage: {
                src: [
                    'dev/js/jquery.min.js',
                    'dev/js/skrollr.min.js',
                    'dev/js/jquery.maskedinput.min.js',
                    'dev/js/script-mainpage.js'
                ],
                dest: 'js/mainpage.js'
            },
            dist_paintball: {
                src: [
                    'dev/js/jquery.min.js',
                    'dev/js/viewport.checker.js',
                    'dev/js/jquery.maskedinput.min.js',
                    'dev/js/lightbox.min.js',
                    'dev/js/slick.min.js',
                    'dev/js/script-paintball.js'
                ],
                dest: 'js/main-paintball.js'
            },
            dist_bathroom: {
                src: [
                    'dev/js/wow.min.js',
                    'dev/js/jquery.min.js',
                    'dev/js/jquery.maskedinput.min.js',
                    'dev/js/slick.min.js',
                    'dev/js/script-bathroom.js'
                ],
                dest: 'js/main-bathroom.js'
            },
            dist_carwash: {
                src: [
                    'dev/js/wow.min.js',
                    'dev/js/jquery.min.js',
                    'dev/js/jquery.maskedinput.min.js',
                    'dev/js/slick.min.js',
                    'dev/js/script-carwash.js'
                ],
                dest: 'js/main-carwash.js'
            },
            dist_restobar: {
                src: [
                    'dev/js/jquery.min.js',
                    'dev/js/slick.min.js',
                    'dev/js/jquery.maskedinput.min.js',
                    'dev/js/lightbox.min.js',
                    'dev/js/script-restobar.js',
                    'dev/js/restobar-canvas.js'
                ],
                dest: 'js/main-restobar.js'
            }
        },
        
        less: {
            development: {
                options: {
                    compress: true,
                    yuicompress: true,
                    optimization: 2,
                    path:["dev/css"]
                },
                files: {
                    "dev/css/style.css": "dev/css/style.less"
                }
            }
        },

        cssmin: {
            options: {
                mergeIntoShorthands: false,
                roundingPrecision: -1
            },
            target: {
                files: {
                    'css/style.css': ['dev/css/animate.css', 'dev/css/lightbox.min.css', 'dev/css/style.css']
                }
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-cssmin');

    grunt.registerTask('default', ['less','concat','cssmin']);
};